<?php
/**
 * @package readlesstitle
 * @copyright 2008-__YEAR__ Parvus
 * @license http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://joomlacode.org/gf/project/cutoff/
 * @author Parvus
 *
 * readless is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * readless is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with readless. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @version $Id: readlesstitle.php 201 2012-11-22 20:07:17Z parvus $
 */

defined( '_JEXEC' ) or die;
jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.utilities.date' );

class ReadLessTitleHelper
{
  /**
   * Checks whether it is allowed to run.
   * This function does not make any modification, escept when in discover
   * mode: @c $article->text will then be set to the discover information.
   * @param article $article the item fetched from the database
   * @param params $params the parameters to use
   * @param dict $options OUT the key 'discover' with a boolean value will be
   *   filled in.
   * @param bool $activeByDefaultOnAllContentItems True if all content items
   *   must be shortened by 'read less', unless explicitly disallowed.
   * @param string $extraDiscoverInfo When Discover mode is active, this string
   *   will be added to the discover information.
   * @return Boolean value
   */
  function Filter( $article, $params, &$options, $activeByDefaultOnAllContentItems = false, $extraDiscoverInfo = '' )
  {
    global $option;
    $current = array();
    $current[ 'component' ] = JRequest::getWord( 'option' );
    $current[ 'view' ] = JRequest::getWord( 'view' );
    $current[ 'viewId' ] = JRequest::getInt( 'id' );
    $current[ 'articleId' ] = ReadLessTitleHelper::GetArticleId( $article );
    $current[ 'articleCategoryId' ] = ReadLessTitleHelper::GetCategoryId( $article );
    $allowed = true;

    if ( $params->get( 'when', '0' ) == '0' )
    {
      /* common usage */

      if ( $activeByDefaultOnAllContentItems )
      {
        $allowedFilters = 'com_content';
        $disallowedFilters = '';
      }
      else
      {
        $allowedFilters = 'com_content:blog, com_content:categories, com_content:category, com_content:featured, com_content:frontpage, com_content:section';
        $disallowedFilters = '';
      }
    }
    else
    {
      /* specific usage */
      $allowedFilters = $params->get( 'allowed', '' );
      $disallowedFilters = $params->get( 'disallowed', '' );
    }

    if ( $activeByDefaultOnAllContentItems and ( $current[ 'component' ] == 'com_content' ) )
    {
      $allowedFilters = 'com_content, ' . $allowedFilters;
    }

    $contexts = array();
    $contextDescriptions = array(); /* Only used to facilitate Discover mode. */
    $possiblyInterchangeable = array(); /* Only used to facilitate Discover mode. */
    /* Category/section/other descriptions have to be explicitly enabled.
     * Do not include the more general compact indications of the current
     * page/article in that case.
     */
    if ( $current[ 'articleId' ] != 0 )
    {
      $contexts[] = $current[ 'component' ];
      $contextDescriptions[] = 'all pages of this component';
      $contexts[] = $current[ 'component' ] . ':' . $current[ 'view' ];
      $contextDescriptions[] = 'all similar pages';
      if ( $current[ 'viewId' ] )
      {
        $contexts[] = $current[ 'component' ] . ':' . $current[ 'view' ] . '=' . $current[ 'viewId' ];
        $contextDescriptions[] = 'all items on this page only';
        $possiblyInterchangeable[] = $contexts[ count( $contexts ) - 1 ];
      }
    }
    $contexts[] = $current[ 'component' ] . ':' . $current[ 'articleId' ];
    $contextDescriptions[] = 'this item only on all pages';
    $contexts[] = $current[ 'component' ] . ':' . 'all-in-' . $current[ 'articleCategoryId' ];
    $contextDescriptions[] = 'all items from category ' . $current[ 'articleCategoryId' ] . ' on all pages';
    $contexts[] = $current[ 'component' ] . ':' . $current[ 'view' ] . ':' . $current[ 'articleId' ];
    $contextDescriptions[] = 'this item only on all similar pages';
    $contexts[] = $current[ 'component' ] . ':' . $current[ 'view' ] . ':' . 'all-in-' . $current[ 'articleCategoryId' ];
    $contextDescriptions[] = 'all items from category ' . $current[ 'articleCategoryId' ] . ' on all similar pages';
    $possiblyInterchangeable[] = $contexts[ count( $contexts ) - 1 ];
    if ( $current[ 'viewId' ] )
    {
      $contexts[] = $current[ 'component' ] . ':' . $current[ 'view' ] . '=' . $current[ 'viewId' ] . ':' . $current[ 'articleId' ];
      $contextDescriptions[] = 'this item only on this page only';
      $contexts[] = $current[ 'component' ] . ':' . $current[ 'view' ] . '=' . $current[ 'viewId' ] . ':' . 'all-in-' . $current[ 'articleCategoryId' ];
      $contextDescriptions[] = 'all items from category ' . $current[ 'articleCategoryId' ] . ' on this page only';
      $possiblyInterchangeable[] = $contexts[ count( $contexts ) - 1 ];
    }

    /* Loop over key (may be active if filter does not match) value (parameter name) pairs */
    $loop = array( false => $allowedFilters, true => $disallowedFilters);
    foreach ( $loop as $defaultAllowed => $filters )
    {
      if ( $filters )
      {
        /* A bit more manipulation is required here: $filters can be given in
         * different formats
         * @li {component}:{view}:id targets a specific article displayed in all the views of the given type.
         * @li {component}:{view}=nr:id targets a specific article displayed in the given view only.
         * @li {component}:id targets a specific article displayed in any view.
         * @li {component}:{view} targets all articles displayed in all the views of the given type.
         * @li {component}:{view}=nr targets all articles displayed in the given view .
         * @li {component} targets all articles of that component.
         * @note If {component}: is not given, com_content is assumed.
         * @note If {view} is not given, it is not checked for.
         * The string manipulations below ensure that all filters start with a component name
         * followed by a view (with or without nr) and/or and id.
         */
        $filters = ',' . JString::strtolower( $filters );
        $search = array( ' ', ',', '+com_', '+' );
        $replace = array( '', ',+', 'com_', 'com_content:' );
        $filters = JString::str_ireplace( $search, $replace, $filters );
        $filterList = explode( ',', $filters );

        $filterAllows = $defaultAllowed;
        foreach ( $contexts as $c )
        {
          if ( in_array( $c, $filterList ) !== FALSE )
          {
            $filterAllows = !/*NOT*/$defaultAllowed;
            $lastMatchingContext = $c;
            break;
          }
        }
        $allowed &= $filterAllows;
      }
      else
      {
        /* There is no restriction set. Retain the default or already determined value for $allowed. */
      }
    }

    $discover = $params->get( 'discover', false );
    if ( $discover )
    {
      $version = new JVersion();
      if ($version->RELEASE >= '1.6')
      {
        $discover = JFactory::getUser()->authorise( 'core.login.admin' );
      }
      else
      {
        /* J1.5 */
        $discover = ( JFactory::getUser()->gid >= 22 );
      }
    }
    if ( $options != NULL )
    {
      $options[ 'discover' ] = $discover; /* Store this value so that others don't need to perform the same logic. */
    }
    if ( $discover )
    {
      $enableordisable = array( true => "disable", false => "enable" );

      $pluginName = "<em>read less title</em>";
      $article->text = "";
      $activeornot = array( true => "<strong>active</strong>", false => "<strong>not active</strong>" );
      $article->text .= "<p>" . $pluginName . " is " . $activeornot[$allowed] . " on this item ";
      if ( $allowed )
      {
        $article->text .= "(provided the contents' length is large enough - this is not checked yet at this point).";
      }
      $article->text .= "<ul>Information you can use to create your own contexts:";
      $article->text .= "<li>current component: <code>" . $current[ 'component' ] . "</code></li>";
      $article->text .= "<li>current view: <code>" . $current[ 'view' ] . "</code></li>";
      $article->text .= "<li>current view id: <code>" . $current[ 'viewId' ] . "</code></li>";
      $article->text .= "<li>current article id: <code>" . $current[ 'articleId' ] . "</code></li>";
      $article->text .= "<li>category id of article: <code>" . $current[ 'articleCategoryId' ] . "</code></li>";
      $article->text .= "</ul></p>";
      if ( isset( $lastMatchingContext ) )
      {
        /* Maybe active, maybe not, but at least one context matched. */
        $article->text .= "The last context you configured that matched the current item is <strong><code>" . $lastMatchingContext . "</code></strong>";
        if ( !/*NOT*/$allowed )
        {
          $article->text .= "<br>If you want to enable the plugin on this item on this page, you minimally need to remove or change this context.";
        }
      }
      else if ( $allowed )
      {
        /* Active, but no context ever matched. */
        $article->text .= "<br/>There are no contexts listed where " . $pluginName . " is allowed to be active, so it is <strong>active by default</strong>.";
      }
      else
      {
        /* Not active, but no context ever matched. */
        $article->text .= "<br/>No context matches the current item, so it is <strong>not active by default</strong>.";
      }
      $article->text .= "</p><ul>If you want to " . $enableordisable[$allowed] . " " . $pluginName . " on ";
      for ( $i = 0; $i < count( $contexts ); $i++)
      {
        $article->text .= "<li>" . $contextDescriptions[$i] . ", you can use the context <code>" . $contexts[$i] . "</code>";
      }
      $article->text .= "</ul>";

      $article->text .= "<p>";
      if ( $current[ 'component' ] == 'com_content' )
      {
        $article->text .= "<strong>Note</strong>: when configuring, you may omit <code>com_content:</code> from the context string.";
        $article->text .= "<br/>";
      }
      if ( $current[ 'articleId' ] == $current[ 'viewId' ] )
      {
        $article->text .= "<strong>Note</strong>: if the view name <code>"
            . $current[ 'view' ]
            . "</code> serves to display a single item/article, the contexts <code>"
            . implode( '</code>, <code>', $possiblyInterchangeable )
            . "</code> yield the same result and are interchangeable.<br/>";
      }
      $article->text .= "<strong>Note</strong>: discover information is only displayed to users with back-end permissions and can be disabled in the back-end.";
      if ( $extraDiscoverInfo )
      {
        $article->text .= "<br/>";
        $article->text .= "<strong>" . $extraDiscoverInfo . "</strong>";
      }
      $article->text .= "</p>";
      $article->introtext = $article->text;
      $article->fulltext = "";
    }

    return $allowed;
  }

  /**
   * Determines if @c $string ends with $lastPart.
   * @param string $string The string to examine
   * @param string $lastPart The substring to find at the end of @c $string
   * @return true if @c $string ends with @c $lastPart, false otherwise
   */
  function EndsWith( $string, $lastPart )
  {
    if ( strlen( $string ) < strlen( $lastPart ) )
    {
      $endsWith = false;
    }
    else
    {
      if ( substr_compare( $string, $lastPart, -1 * strlen( $lastPart ) ) )
      {
        $endsWith = false;
      }
      else
      {
        $endsWith = true;
      }
    }
    return $endsWith;
  }

  /**
   * Determines the length of the given article text.
   * @param string $htmltext The html text to consider.
   * @param string $lengthUnit Unit of the length to determine. One of 'char',
   *   'word', 'sentence', 'paragraph'.
   * @param bool $end OUT If true, the last part of @c htmlText ends the
   *   ongoing unit.
   * @return the length expressed in the given unit.
   */
  function DetermineLength( $htmltext, $lengthUnit, &$end )
  {
    /* Subsequent whitespace must be counted as one when $lengthUnit equals
     * 'char'. But other units need this as well: e.g. a non-breaking space
     * or a tab at the end of the $htmlText indicates the last word is
     * finished (trim does not remove non-breaking spaces).
     */
    $text = preg_replace( '/[\s\xc2\xa0]+/mis', ' ', $htmltext );

    switch ( $lengthUnit )
    {
      case 'sentence':
        /* Length is calculated incrementally and needs to be initialized.
         * Calculation is done using a list of sentences.
         * Exclude empty sentences, exclude sentences with only markup.
         * The text is also trimmed to determine afterwards whether the last
         * sentence has ended.
         * Paragraph, row and list item demarcations also mark the end of a sentence.
         * Ensure that all sentence endings can be treated alike.
         */
        $search = array( '</p>', '</li>', '</dt>', '</dl>', '</tr>', '#', '.', '?', '!', '¿' );
        $replace = array( '.', '.', '.', '.', '.', '_', '#', '#', '#', '#' );
        $text = JString::str_ireplace( $search, $replace, $text );
        $text = trim( strip_tags( $text ) );
        $length = 0;
        foreach ( explode( '#', $text ) as $sentence )
        {
          if ( preg_split( '/\s+/', $sentence, -1, PREG_SPLIT_NO_EMPTY ) )
          {
            $length++;
          }
        }
        $end = self::EndsWith( $text, '#' );
        break;

      case 'paragraph':
        /* Calculation is done using a list of non-empty paragraphs.
         * Exclude empty paragraphs, exclude paragraps with only markup.
         * Also, punctuation characters are excluded.
         * The text is trimmed first to determine whether the last paragraph
         * has ended afterwards.
         * Table and list demarcations also mark the end of a sentence.
         * Ensure that all paragraph endings can be treated alike.
         */
        $search = array( '</ul>', '</ol>', '</dl>' );
        $replace = array( '</p>', '</p>', '</p>' );
        $text = JString::str_ireplace( $search, $replace, $text );
        $text = trim( $text );
        $length = 0;
        foreach ( explode( '</p>', $text ) as $paragraph )
        {
          $paragraph = strip_tags( $paragraph );
          if ( preg_split( '/\s+/', $paragraph, -1, PREG_SPLIT_NO_EMPTY ) )
          {
            $length++;
          }
        }
        $end = self::EndsWith( $text, '</p>' );
        break;

      case 'word':
        /* Paragraph, row and list item demarcations also mark the end of a sentence.
         * Ensure that all word endings can be treated alike.
         */
        $search = array( '</p>', '</li>', '</dt>', '</dl>', '</tr>', '.', '?', '!', '¿' );
        $replace = array( ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' );
        $text = JString::str_ireplace( $search, $replace, $text );
        $text = strip_tags( $text );
        $length = count( preg_split( '/\s+/', $text, -1, PREG_SPLIT_NO_EMPTY ) );
        $end = self::EndsWith( $text, ' ' );
        break;

      case 'char':
      default:
        /* Calculation only needs the plaintext. */
        $text = strip_tags( $text );
        $length = JString::strlen( $text );
        $end = true;
        break;
    }

    return $length;
  }

  /**
   * Returns the id of the article this plugin is being called upon.
   * Works for com_content and com_eventlist items,
   * and others (list?)
   * @param $article
   * @return A number.
   */
  function GetArticleId( &$article )
  {
    $id = 0;
    /* Order of for loop is important: the last field that exists 'wins'. */
    foreach ( array( 'id', 'cid', 'did' ) as $field )
    {
      if ( isset( $article->$field ) )
      {
        $id = (int)$article->$field;
      }
    }
    return $id;
  }

  /**
   * Returns the id of the category of the article this plugin is being called
   * upon.
   * Works for com_content and com_eventlist items,
   * and others (list?)
   * @param $article
   * @return A number.
   */
  function GetCategoryId( &$article )
  {
    $id = 0;
    /* Order of for loop is important: the last field that exists 'wins'. */
    foreach ( array( 'catid', 'catsid' ) as $field )
    {
      if ( isset( $article->$field ) )
      {
        $id = (int)$article->$field;
      }
    }
    return $id;
  }

  /**
   *
   * @param string $plainText
   * @param integer $length
   * @param string $lengthUnit Unit of the length. One of 'char',
   *   'word', 'sentence', 'paragraph'.
   * @param bool $retainWholeWords Only used when $lengthUnit equals 'char'
   */
  function Substr( $plainText, $length, $lengthUnit, $retainWholeWords = false )
  {
    $substr = $plainText;
    switch ( $lengthUnit )
    {
      case 'sentence':
        $substrLength = 0;
        $matches = preg_split( '/([.?!¿]+)/mis', $plainText, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_OFFSET_CAPTURE );
        $i = 0;
        while ( ( $i < count( $matches ) ) and ( $length > 0 ) /* If there are still sentences to be included. */ )
        {
          $offset = 0; /* Use the data from the sentence by default. */
          if ( $i + 1 < count( $matches ) )
          {
            $offset = 1; /* Use the data from the delimiter after the sentence. */
          }
          $substrLength = $matches[ $i + $offset ][1]; /* Start position */
          $substrLength += JString::strlen( $matches[ $i + $offset ][0] ); /* Length of matched string */

          $i += 2;
          $length--;
        }
        $substr = JString::substr( $plainText, 0, $substrLength);
        break;

      case 'paragraph':
        /* Default value is correct. Nothing to do. */
        break;

      case 'word':
        $words = preg_split( '/\s+/', $plaintext, $length + 1, PREG_SPLIT_NO_EMPTY );
        $words = array_slice( $words, 0, $length );
        $substr = implode( ' ', $words );
        break;

      case 'char':
      default:
        /* Subsequent whitespace must be counted as one. */
        $substrLength = 0;
        foreach ( preg_split( '/\s+/', $plainText, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_OFFSET_CAPTURE ) as $match )
        {
          if ( $length > 0 ) /* If there are still characters to be included. */
          {
            $substrLength = $match[1];
            $wordLength = JString::strlen( $match[0] );
            if ( $retainWholeWords or ( $length >= $wordLength ) )
            {
              $substrLength += $wordLength;
            }
            else
            {
              $substrLength += $length;
            }
            $length -= $wordLength;
            $length--; /* Whitespace is counted as one. */
          }
        }
        $substr = JString::substr( $plainText, 0, $substrLength);
        break;
    }

    return $substr;
  }

  /**
   * Determines the correct url to the corresponding thumbnail.
   * If the thumbnail does not exist, it is created
   * @param string $url The path to the image
   * @param dict $minimum Associative array, with keys 'width', 'height', 'ratio',
   *   and values 0 or positive numbers, expressed in pixels.
   *   Only looked at when the thumbnail does not exist yet and has to be
   *   created.
   * @param dict $crop Associative array, with keys 'horizontal_position',
   *   'vertical_position' and values 'left', 'right', 'center' or 'no'.
   * @param int $thumbWidth A number. May be zero or negative. When positive, it
   *   indicates the maximum width of the resized thumbnail. Else, there is no
   *   restriction on image width, and will be chosen in function of the
   *   height.
   * @param int $thumbHeight A number. May be zero or negative. When positive, it
   *   indicates the maximum height of the resized thumbnail. Else, there is
   *   no restriction on image height, and will be chosen in function of the
   *   width.
   * @param int $lifetime The lifetime of the thumbnail in seconds to set when it
   *   is created by calling this function. Not used to check if the existing
   *   thumbnail is still valid. Default: 4 weeks (2419200 seconds).
   * @return false if an error occurred or if the given $url is incorrect. The
   *   path to the thumbnail otherwise.
   */
  function GetThumbnail( $url, $minimum, $crop, & $thumbWidth, & $thumbHeight, $lifetime = 2419200 )
  {
    $path = JPATH_CACHE . '/plg_readlesstext/';
    if ( !/*NOT*/@file_exists( $path ) )
    {
      @mkdir( $path );
    }
    $string = $url . $minimum[ 'width' ] . $minimum[ 'height' ] . $minimum[ 'ratio' ]
    . $crop[ 'horizontal_position' ] . $crop[ 'vertical_position' ]
    . $thumbWidth . $thumbHeight;
    $thumbnailPath = $path . md5( $string );
    $noThumbnailPath = $thumbnailPath . '.x';
    /* .ext appended below */

    /* Transform the local path into a url */
    $parts = explode( JPATH_BASE . '/', $thumbnailPath, 2 );
    $thumbnailUrl = JURI::base() . $parts[1];

    if ( @file_exists( $noThumbnailPath ) )
    {
      /* The image resource $url has been examined during a previous execution.
       * According to the settings, it is not fit to serve as a thumbnail.
      */
      $thumbnailUrl = false;
    }
    else
    {
      $type = 0;
      if ( function_exists( 'exif_imagetype' ) )
      {
        $type = @exif_imagetype( $url );
      }
      if ( !/*NOT*/$type )
      {
        /* Fallback: try to determine the correct image type by checking for an extension in the url. */
        $ext = '';
        $parts = explode( '.', $url );
        if ( count( $parts ) > 1 )
        {
          $parts = explode( '?', $parts[ count( $parts ) - 1 ], 2 );
          $ext = JString::strtolower( $parts[ 0 ] );
        }
        if ( array_key_exists( $ext, ReadLessTitleHelper::$_extToType ) )
        {
          $type = ReadLessTitleHelper::$_extToType[ $ext ];
        }
      }
      if ( $type and array_key_exists( $type, ReadLessTitleHelper::$_image ) )
      {
        $thumbnailPath .= ReadLessTitleHelper::$_image[ $type ][ 'ext' ];
        $thumbnailUrl .= ReadLessTitleHelper::$_image[ $type ][ 'ext' ];
        if ( @file_exists( $thumbnailPath ) )
        {
          /* Thumbnail already exists.
           *
          * The image resource $url has been examined during a previous
          * execution. According to the settings, it is fit to serve
          * as a thumbnail.
          *
          * Done!
          */
          $sizeArray = @getimagesize( $thumbnailPath );
          $thumbWidth = $sizeArray[ 0 ];
          $thumbHeight = $sizeArray[ 1 ];
        }
        else if ( !/*NOT*/@is_dir( $path ) or !/*NOT*/@is_writable( $path ) )
        {
          /* Insufficient write permissions. Use fall-back. */
          $thumbnailUrl = $url;
        }
        else
        {
          $image = call_user_func( ReadLessTitleHelper::$_image[ $type ][ 'load' ], $url );

          $width = max( 1, @imagesx( $image ) ); /* Ensure a division is possible. */
          $height = max( 1, @imagesy( $image ) ); /* Ensure a division is possible. */
          $ratio = min( $width / $height, $height / $width );

          if ( !/*NOT*/$image
              or ( $width < $minimum[ 'width' ] )
              or ( $height < $minimum[ 'height' ] )
              or ( $ratio < $minimum[ 'ratio' ] ) )
          {
            /* Thumbnail may not be created.
             * According to the settings, it is not fit to serve as a thumbnail.
            */

            @file_put_contents( $noThumbnailPath, time() );
            /* The expiration information is not used directly, but it is still
             * added to allow Joomla's core garbage collection functionality to work.
            */
            $expirePath = $noThumbnailPath . '_expire';
            @file_put_contents( $expirePath, ( time() + $lifetime ) );

            $thumbnailUrl = false;
          }
          else
          {
            /* Find the resized dimensions, keeping the proportions.
             *
            * There are four different ways to resize:
            * A: resize full width to thumbnail width,
            *     resize height with same ratio,
            *         crop height to thumbnail height (top, bottom, evenly both)
            * B: resize full height to thumbnail height,
            *     resize width with same ratio,
            *         crop width to thumbnail width (left, right, evenly both)
            * C: resize full width to thumbnail width,
            *     resize height with same ratio,
            *         resized height <= thumbnail height
            * D: resize full height to thumbnail height,
            *     resize width with same ratio,
            *         resized width <= thumbnail width
            *
            * Based on
            * - the actual image dimensions: ix, iy
            * - the desired thumbnail dimensions: tx, ty
            * - the crop options:
            *     crop horizontal: yes (left/right/evenly) or no (do not crop horizontally)
            *     crop vertical: yes (left/right/evenly) or no (do not crop vertically)
            * we need to determine the resize factor.
            *
            * Determine the horizontal and vertical ratio's: rx, ry
            * - If rx < ry: If resized using ry, the horizontal width will be
            *     greater than the thumbnail width. So either the width must
            *     be cropped (if allowed), either the image must be resized
            *     using rx (and thus the resized height will be less than the
                *     desired thumbnail height ty).
            * - If rx == ry: highly unlikely. Crop options are not needed
            *     here. Just resize using the single resize factor that was
            *     calculated.
            * - If rx > ry: Similar to the first case.
            *     Replace width <> height, rx <> ry and ty <> tx
            * Thus:
            * rx, ry = tx/ix, ty/iy
            * rx < ry
            *   ? crop horizontal ? r = ry : r = rx
            *   : crop vertical ? r = rx : r = ry
            *
            * The image width to use is equal to r * tx
            * The image height to use is equal to r * ty
            *
            * Determine the start positions sx, sy: everything lower and
            * everything higher than that plus the image width/height will be
            * thrown away (cropped).
            * Default value is 0, 0, to be used when cutting on the
            * right/bottom, or when cropping is disabled.
            * If there is something to be thrown away, i.e.
            * if r * ix > tx
            *   cut left, retain right ? sx = ix - tx / r
            *   cut right, retain left ? sx = 0
            *   cut evenly ? sx = (ix - tx / r) / 2
            * Likewise for sy
            *
            * Done!
            */

            /* -------------------------------------------------------------------------------- */
            /* Determine ratio to use */
            if ( $thumbWidth > 0 )
            {
              $resizeFactorWidth = min( 1, $thumbWidth / $width );
            }
            else
            {
              $thumbWidth = $width;
              $resizeFactorWidth = 1;
            }
            if ( $thumbHeight > 0 )
            {
              $resizeFactorHeight = min( 1, $thumbHeight / $height );
            }
            else
            {
              $thumbHeight = $height;
              $resizeFactorHeight = 1;
            }
            $resizeFactor = 1; /* Default value */
            if ( $resizeFactorWidth < $resizeFactorHeight )
            {
              /* Width is (relatively) greater than the height. */
              if ( in_array( $crop[ 'horizontal_position' ], array( 'left', 'right', 'center' ) ) )
              {
                /* Horizontal cropping is allowed. We may resize less,
                 * and crop the extraneous part.
                */
                $resizeFactor = $resizeFactorHeight;
              }
              else
              {
                /* Horizontal cropping is not allowed. The full width must be
                 * resized: the resized height will be less than the intended
                * thumbnail height.
                */
                $resizeFactor = $resizeFactorWidth;
              }
            }
            else
            {
              /* Vertical cropping is allowed. We may resize less,
               * and crop the extraneous part.
               */
              if ( in_array( $crop[ 'vertical_position' ], array( 'top', 'bottom', 'center' ) ) )
              {
                $resizeFactor = $resizeFactorWidth;
              }
              else
              {
                /* Vertical cropping is not allowed. The full height must be
                 * resized: the resized width will be less than the intended
                 * thumbnail width.
                 */
                $resizeFactor = $resizeFactorHeight;
              }
            }

            /* -------------------------------------------------------------------------------- */
            /* Determine start positions */

            $horizontalStart = 0; /* Default value */
            $usedWidth = min( $width, $thumbWidth / $resizeFactor );
            $thumbWidth = intval( ( $usedWidth * $resizeFactor ) + 0.01 );
            if ( $usedWidth + 1 < $width )
            {
              switch ( $crop[ 'horizontal_position' ] )
              {
                case 'center':
                  $horizontalStart = max( 0, ( $width - $usedWidth ) / 2 );
                  break;

                case 'right':
                  $horizontalStart = max( 0, $width - $usedWidth );
                  break;

                case 'left':
                  /* $horizontalStart remains 0 */
                  break;

                default:
                  /* Do not crop the height after all. We should never get here! */
                  break;
              }
            }
            $verticalStart = 0; /* Default value */
            $usedHeight = min( $height, $thumbHeight / $resizeFactor );
            $thumbHeight = intval( ( $usedHeight * $resizeFactor ) + 0.01 );
            if ( $usedHeight + 1 < $height )
            {
              switch ( $crop[ 'vertical_position' ] )
              {
                case 'center':
                  $verticalStart = max( 0, ( $height - $usedHeight ) / 2 );
                  break;

                case 'bottom':
                  $verticalStart = max( 0, $height - $usedHeight );
                  break;

                case 'top':
                  /* $verticalStart remains 0 */
                  break;

                default:
                  /* Do not crop the height after all. We should never get here! */
                  break;
              }
            }

            /* -------------------------------------------------------------------------------- */
            /* Create thumbnail */

            $thumbnail = call_user_func( ReadLessTitleHelper::$_image[ $type ][ 'create' ], $thumbWidth, $thumbHeight );
            if ( $type == 1 /* IMAGETYPE_GIF */ )
            {
              /* Make the thumbnail initially transparent if the original was transparent too.
               * Otherwise, fill it initially up with all white.
              */
              $transparentColorIdentifier = @imagecolortransparent( $image );
              if ( $transparentColorIdentifier >= 0 )
              {
                $colors = @imagecolorsforindex( $image, $transparentColorIdentifier );
                $transcolorindex = @imagecolorallocate( $thumbnail, $colors[ 'red' ], $colors[ 'green' ], $colors[ 'blue' ] );
                @imagefill( $thumbnail, 0, 0, $transcolorindex );
                @imagecolortransparent( $thumbnail, $transcolorindex ); /* Needed? */
              }
              else
              {
                $whiteColorIdentifier = @imagecolorallocate( $thumbnail, 255, 255, 255 );
                @imagefill( $thumbnail, 0, 0, $whitecolorindex);
              }
            }

            if ( ReadLessTitleHelper::$_image[ $type ][ 'create_alpha' ] )
            {
              call_user_func( ReadLessTitleHelper::$_image[ $type ][ 'create_alpha' ], $thumbnail, false );
            }
            call_user_func( ReadLessTitleHelper::$_image[ $type ][ 'copy' ], $thumbnail, $image,
                0, 0, $horizontalStart, $verticalStart,
                $thumbWidth, $thumbHeight, $usedWidth, $usedHeight );
            if ( ReadLessTitleHelper::$_image[ $type ][ 'save_alpha' ] )
            {
              call_user_func( ReadLessTitleHelper::$_image[ $type ][ 'save_alpha' ], $thumbnail, true );
            }
            call_user_func( ReadLessTitleHelper::$_image[ $type ][ 'save' ], $thumbnail, $thumbnailPath );

            /* The expiration information is not used directly, but it is still
             * added to allow Joomla's core garbage collection functionality to work.
            */
            $expirePath = $thumbnailPath . '_expire';
            @file_put_contents( $expirePath, ( time() + $lifetime) );
          }
        }
      }
      else
      {
        /* To me, the remaining image types are esoteric. Some of them I never
         * even heard of.
         * OR
         * Determining the image type failed.
         */
        @file_put_contents( $noThumbnailPath, time() );
        /* The expiration information is not used directly, but it is still
         * added to allow Joomla's core garbage collection functionality to work.
         */
        $expirePath = $noThumbnailPath . '_expire';
        @file_put_contents( $expirePath, ( time() + $lifetime ) );

        $thumbnailUrl = false;
      }
    }

    return $thumbnailUrl;
  }

  static $_extToType = array(
      'gif' => 1 /* IMAGETYPE_GIF */,
      'jpg' => 2 /* IMAGETYPE_JPEG */,
      'jpeg' => 2 /* IMAGETYPE_JPEG */,
      'png' => 3 /* IMAGETYPE_PNG */
  );

  static $_image = array(
      1 /* IMAGETYPE_GIF */ => array(
          'ext' => '.gif',
          'load' => 'imagecreatefromgif',
          'create' => 'imagecreate',
          'create_alpha' => '',
          'copy' => 'imagecopyresampled',
          'save_alpha' => '',
          'save' => 'imagegif'
      ),
      2 /* IMAGETYPE_JPEG */ => array(
          'ext' => '.jpg',
          'load' => 'imagecreatefromjpeg',
          'create' => 'imagecreatetruecolor',
          'create_alpha' => '',
          'copy' => 'imagecopyresampled',
          'save_alpha' => '',
          'save' => 'imagejpeg'
      ),
      3 /* IMAGETYPE_PNG */ => array(
          'ext' => '.png',
          'load' => 'imagecreatefrompng',
          'create' => 'imagecreatetruecolor',
          'create_alpha' => 'imagealphablending',
          'copy' => 'imagecopyresampled',
          'save_alpha' => 'imagesavealpha',
          'save' => 'imagepng'
      ) );
}

class ReadLessTitleExpand
{
  /**
   * Prepares a future call to Expand(). Based on the given arguments, the
   * expandables are set.
   * @param $article RO.
   * @param $plaintext RO. The plain text version of the full, unshortened article.
   * @param $dateFormat Determines the format for the date fields to expand
   * in $fix.
   * @param $overrides Overrides information deduced from $article with given
   * values. Keys are one of {author}, {author_id}, {words}, {created},
   * {modified}, {publish_up}, {hits}, {category}, {category_id}, {id},
   * {component}, {title}, {url}
   */
  function SetExpandables( $article, $plaintext, $dateFormat, $overrides = array() )
  {
    $authorId = 0;
    if ( isset( $article->created_by ) )
    {
      $authorId = $article->created_by;
    }

    $author = "";
    if ( isset( $article->author ) and $article->author )
    {
      $author = $article->author;
    }
    if ( isset( $article->created_by_alias ) and $article->created_by_alias )
    {
      $author = $article->created_by_alias;
    }

    $words = count( preg_split( '/\s+/', $plaintext, -1, PREG_SPLIT_NO_EMPTY ) );
    if ( !/*NOT*/$words )
    {
      $words = '';
    }

    $created = '';
    if ( isset( $article->created ) )
    {
      $created = new JDate( $article->created );
      $created = $created->toFormat( $dateFormat );
    }

    $modified = $created;
    if ( isset( $article->modified ) and ( $article->modified != '0000-00-00 00:00:00' ) )
    {
      $modified = new JDate( $article->modified );
      $modified = $modified->toFormat( $dateFormat );
    }

    $publishUp = "";
    if ( isset( $article->publish_up ) )
    {
      $publishUp = new JDate( $article->publish_up );
      $publishUp = $publishUp->toFormat( $dateFormat );
    }

    $hits = "";
    if ( isset( $article->hits ) )
    {
      $hits = $article->hits;
    }

    $categoryId = ReadLessTitleHelper::GetCategoryId( $article );

    $category = "";
    if ( isset( $article->category ) )
    {
      $category = $article->category;
    }

    $title = "";
    if ( isset( $article->title ) )
    {
      $title = $article->title;
    }

    $this->_expandables = array(
        '{author}' => $author,
        '{author_id}' => $authorId,
        '{author-id}' => $authorId,
        '{words}' => $words,
        '{created}' => $created,
        '{modified}' => $modified,
        '{published}' => $publishUp,
        '{publish_up}' => $publishUp,
        '{publish-up}' => $publishUp,
        '{hits}' => $hits,
        '{cat}' => $categoryId,
        '{catid}' => $categoryId,
        '{category}' => $categoryId,
        '{category_id}' => $categoryId,
        '{category-id}' => $categoryId,
        '{title}' => $title,
        '{id}' => ReadLessTitleHelper::GetArticleId( $article ),
        '{component}' => JRequest::getWord( 'option' ),
        '{option}' => JRequest::getWord( 'option' ) );
    $this->_expandables = array_merge( $this->_expandables, $overrides );
  }

  /**
   * Expands the string according to the expandables, set in a previous call to SetExpandables
   * @param string $string
   */
  function Expand( $string )
  {
    return JString::str_ireplace( array_keys( $this->_expandables ), array_values( $this->_expandables ), $string );
  }

  var $_expandables = array();
}

class plgContentReadLessTitle extends JPlugin
{
  function __construct( &$subject, $config )
  {
    parent::__construct($subject, $config);
    $this->loadLanguage();
  }

  /**
   * Entry function. Will be called each time some article text
   * is to be prepared for display.
   * @return void
   * @{
   */
  function onContentBeforeDisplay( $context, &$article, &$params, $limitstart = 0 )
  {
    /* The two blocks below are a workaround for issues in the Joomla core.
     * - In a 'featured' view, the fulltext is not set or empty, even when it
     *   is present in the database
     * - In a single article view, the readmore property is not set.
     * Seen in 1.6.6 and 1.7.2
     *
     * It also ensures that when this plugin is invoked on other components (e.g. com_media),
     * There are no php notices.
     */
    foreach ( array( 'fulltext', 'readmore' ) as $var )
    {
      if ( !/*NOT*/ isset( $article->$var ) )
      {
        $article->$var = '';
      }
    }
    if ( $article->readmore and ( !/*NOT*/ $article->fulltext ) )
    {
      $db = JFactory::getDBO();
      $query = "SELECT c.fulltext
      FROM #__content c
      WHERE c.id = " . $article->id;
      $db->setQuery( $query );
      $article->fulltext = $db->loadResult();
    }
    $this->_ReadLessTitle( $article );
  }
  //   onContentPrepare is not used: the call is too limited.
  //   $article only contains a text field, not the id and other needed fields.
  //   function onContentPrepare( $context, &$article, &$params, $limitstart = 0 )
  //   {
  //     $this->_ReadLessText( $article );
  //   }
  function onPrepareContent( &$row, &$params, $page )
  {
    /* J1.5 */
    $this->_ReadLessTitle( $row );
  }
  /** @} */

  function _ReadLessTitle( &$article )
  {
    if ( isset( $article->title ) and $article->title )
    {
//      jimport( 'joomla.error.profiler' );
//      $profiler = new JProfiler();

      $dontcare = array();
      if ( ReadLessTitleHelper::Filter( $article, $this->params, $dontcare ) )
      {
        /* We must try and shorten the title. */
        $isShortened = false;

        $roundBrackets = $this->params->get( 'stripRoundBrackets', false );
        $squareBrackets = $this->params->get( 'stripSquareBrackets', false );
        $accolades = $this->params->get( 'stripAccolades', false );
        if ( self::_StripTitle( $article->title, $roundBrackets, $squareBrackets, $accolades ) )
        {
          $isShortened = true;
        }

        if ( self::_ShortenTitle( $article->title ) )
        {
          $isShortened = true;
        }

        /* Try to case-correct the entire title */
        $if = $this->params->get( 'convertTitleIf', 'upper' );
        $to = $this->params->get( 'convertTitleTo', 'sentence' );
        $title = self::_CorrectCase( $article->title, $if, $to );

        /* Try to case-correct each word of the title */
        $if = $this->params->get( 'convertWordsIf', 'upper' );
        $to = $this->params->get( 'convertWordsTo', 'sentence' );
        if ( $if != 'never' )
        {
          $words = preg_split( '/\s+/', $title, -1, PREG_SPLIT_NO_EMPTY );
          for ( $i = 0; $i < count( $words ); $i++ )
          {
            $words[ $i ] = self::_CorrectCase( $words[ $i ], $if, $to );
          }
          $title = implode( ' ', $words );
        }
        $article->title = $title;

        /* Prepare determining the pre- and suffix */
        $expand = new ReadLessTitleExpand();
        $options = array();
        $options[ 'dateFormat' ] = $this->params->get( 'dateFormat', '%m/%d' );
        $options[ 'prefix' ] = $this->params->get( 'prefix', '' );
        $options[ 'suffix' ] = $this->params->get( 'suffix', '' );
        self::_DetermineArticleText( $article );
        $expand->SetExpandables( $article, $this->_plaintext, $options[ 'dateFormat' ] );

        /* Determine the pre- and suffix */
        $prefix = '';
        $addPrefix = $this->params->get( 'addPrefix', 'when_active' );
        if ( ( $addPrefix == 'when_active' )
            or ( ( $addPrefix == 'when_shortened' ) and $isShortened ) )
        {
          $prefix = $expand->expand( $options[ 'prefix' ] );
        }
        $suffix = '';
        $addSuffix = $this->params->get( 'addSuffix', 'when_shortened' );
        if ( ( $addSuffix == 'when_active' )
            or ( ( $addSuffix == 'when_shortened' ) and $isShortened ) )
        {
          $suffix = $expand->expand( $options[ 'suffix' ] );
        }
        $article->title = $prefix . $article->title . $suffix;
      }
//      $article->text .= $profiler->mark( ' Profiler ' );
    }
  }

  /**
   * Determine both the plaintext and the html version of the full article.
   * @param article $article RO
   * @return void
   * @post The private variables plaintext and htmltext have been set.
   */
  function _DetermineArticleText( &$article )
  {
    /* Determine both the plaintext and the html version of the intro and the
     * full field. Be extra careful, so that other components do not give PHP
     * notices and warnings.
     * - Some components do not have the introtext and/or the fulltext field,
     *   but only the text field,
     *   e.g. ??
     * - Others have none of the three, but use a special name (why o why),
     *   e.g. in com_eventlist:
     *     datdescription for an event.
     *     catdescription for a category.
     *     locdescription for a venue.
     *     description for a group.
     * - whereas others have all three of them, and full info is to be
     *   fetched from introtext and fulltext.
     */
    $introtext = '';
    $fulltext = '';
    if ( isset( $article->introtext ) )
    {
      $introtext = $article->introtext;
    }
    if ( isset( $article->fulltext ) )
    {
      $fulltext = $article->fulltext;
    }
    if ( !/*NOT*/$introtext and !/*NOT*/$fulltext)
    {
      if ( isset( $article->text ) and $article->text )
      {
        $fulltext = $article->text;
      }
    }
    if ( !/*NOT*/$introtext and !/*NOT*/$fulltext)
    {
      /* No text has been found yet. Check if it is stored in a variable named
       * xxxdescription, like eventlist does.
       */
      $values = array();
      foreach ( get_object_vars( $article ) as $key => $value )
      {
        if ( strstr( $key, 'description') !== false )
        {
          $values[] = $value;
        }
      }
      /* Only accept the text from a xxxdescription when it is teh only one
       * found: e.g. eventlist uses one object to store the text of both
       * the event and the venue, and it is impossible to chose the correct
       * one.
       */
      if ( count() == 1 )
      {
        $fulltext = $values[0];
      }
    }
    $this->_plaintext = strip_tags( $introtext ) . ' ' . strip_tags( $fulltext );
    $this->_htmltext = $introtext . ' ' . $fulltext;
  }

  /**
   * Shortens the title.
   * @param string $title IN OUT The string to shorten in-place.
   * @return boolean True if $article->title has been changed (shortened).
   */
  function _ShortenTitle( &$title )
  {
    $originalLength = JString::strlen( $title );
    $minimumLength = $this->params->get( 'minimumTitleLength', 30 );
    if ( $originalLength >= $minimumLength )
    {
      $cutOffTitleLength = max( 1, $this->params->get( 'cutOffTitleLength', 25 ) ) ;
      $retainWholeWords = $this->params->get( 'retainWholeWords', false );
      $title = ReadLessTitleHelper::Substr( $title, $cutOffTitleLength, 'char', $retainWholeWords );
    }
    return ( JString::strlen( $title ) < $originalLength );
  }

  /**
   * Checks if the casing of a string must be adapted, and adapts it.
   * @param string $string The string - word or sentence - that must be
   *   adapted
   * @param string $if One of 'lower', 'upper', 'always', 'never'. Describes
   *   the condition $string must comply to before an modification is made
   * @param string $to One of 'lower', 'upper', 'title', 'sentence',
   *   'sentence_strict'. Describes the action to perform on $string when the
   *   condition @c $if is met.
   * @return The case-adjusted string
   */
  function _CorrectCase( $string, $if, $to )
  {
    switch ( $if )
    {
      case 'lower':
        $do = ( $string == mb_convert_case( $string, MB_CASE_LOWER, 'UTF-8' ) );
        break;

      case 'upper':
        $do = ( $string == mb_convert_case( $string, MB_CASE_UPPER, 'UTF-8' ) );
        break;

      case 'always':
        $do = true;
        break;

      case 'never':
      default:
        $do = false;
        break;
    }
    if ( $do )
    {
      switch ( $to )
      {
        case 'lower':
          $string = mb_convert_case( $string, MB_CASE_LOWER, 'UTF-8' );
          break;

        case 'upper':
          $string = mb_convert_case( $string, MB_CASE_UPPER, 'UTF-8' );
          break;

        case 'title':
          $string = mb_convert_case( $string, MB_CASE_TITLE, 'UTF-8' );
          break;

        case 'sentence_strict':
          $string = mb_convert_case( $string, MB_CASE_LOWER, 'UTF-8' );
          $string[0] = mb_convert_case( $string[0], MB_CASE_UPPER, 'UTF-8' );

        case 'sentence':
        default:
          $string[0] = mb_convert_case( $string[0], MB_CASE_UPPER, 'UTF-8' );
          break;
      }
    }

    return $string;
  }

  /**
   * Removes (portions), [portions] and/or {portions} of the given string,
   * @param string $title IN OUT The string to operate on.
   * @param bool $roundBrackets true when round brackets () and the text in
   *   between must be removed.
   * @param bool $squareBrackets true when square brackets [] and the text in
   *   between must be removed.
   * @param bool $accolades true when accolades {} and the text in between must
   *   be removed.
   * @return The adapted string
   */
  function _StripTitle( &$title, $roundBrackets, $squareBrackets, $accolades )
  {
    $stripped = false;

    foreach ( array( array( $roundBrackets, '\(', '\)' ),
            array( $squareBrackets, '\[', '\]' ),
            array( $accolades, '{', '}' ) ) as $delimiters )
    {
      if ( $delimiters[0] )
      {
        $search = array( self::_patternOpeningReplacer, self::_patternClosingReplacer );
        $replace = array( $delimiters[1], $delimiters[2] );
        $pattern = JString::str_ireplace( $search, $replace, self::_pattern );
        $count = 0;
        $title = preg_replace( $pattern, '', $title, -1, $count );
        if ( $count )
        {
          $stripped = true;
        }
      }
    }
    return $stripped;
  }

  /**
   * Used to find delimited substrings like (abc)
   *
   * A match found using this regular expression will return at each index:
   * [0] The complete token code, inclusive the opening character and the closing token.
   *
   * @note the xxx and yy are to be replaced
   * - replace xxx with [ or { or ... to catch the start of a token
   * - replace yyy with ] or } or ... to catch the end of a token
   */
  const _pattern = "/xxx.*?yyy/mis";
  const _patternOpeningReplacer = 'xxx';
  const _patternClosingReplacer = 'yyy';
}
?>
