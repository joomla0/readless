<?php
/**
 * @package readlesstext
 * @copyright __YEAR__ Parvus
 * @license http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://joomlacode.org/gf/project/cutoff/
 * @author Parvus
 *
 * readless is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * readless is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with readless. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @version $Id$
 */

defined( '_JEXEC' ) or die;
jimport( 'joomla.plugin.plugin' );

global $readlesstextagainErrorMessage;
if ( file_exists( dirname(__FILE__) . '/../readlesstext/readlesstextmain.php' ) )
{
  require_once( dirname(__FILE__) . '/../readlesstext/readlesstextmain.php' );
  $readlesstextagainErrorMessage = false;
}
else
{
  $readlesstextagainErrorMessage = 'PLG_CONTENT_READLESSTEXTAGAIN_DEPENDENCY_MISSING';
}

class plgContentReadLessTextAgain extends JPlugin
{
  function __construct( &$subject, $config )
  {
    global $readlesstextagainErrorMessage;
    if ( $readlesstextagainErrorMessage )
    {
      parent::__construct( $subject, $config );
      $this->loadLanguage();

      /* Can not function. */
    }
    else
    {
      $config[ 'params' ] = self::_CorrectParams( $config[ 'params' ] );

      parent::__construct( $subject, $config );
      $this->loadLanguage();

      /* Create main instance */
      JPlugin::loadLanguage( 'plg_content_readlesstext' );
      $this->_main = new ReadLessTextMain( $subject, $config );
    }
  }

  /**
   * Entry function. Will be called each time some article text
   * is to be prepared for display.
   * @return void
   */
  function onContentBeforeDisplay( $context, &$article, &$params, $limitstart = 0 )
  {
    global $readlesstextagainErrorMessage;
    $app = JFactory::getApplication();
    $scope = $app->scope;
    if ( !/*NOT*/key_exists( $scope, self::$_callCount ) )
    {
      self::$_callCount[ $scope ] = 0;
    }

    if ( $readlesstextagainErrorMessage )
    {
      if ( self::$_callCount[ $scope ] == 0 ) /* Only report this error once per page (and per scope, but that is still limited). */
      {
//         if ( !/*NOT*/JFactory::getUser()->authorise( 'core.login.admin' ) )
//         {
//           $readlesstextagainErrorMessage = 'PLG_CONTENT_READLESSTEXTAGAIN_DEPENDENCY_WARNING';
//         }
        $app = JFactory::getApplication();
        $app->enqueueMessage( JText::_( $readlesstextagainErrorMessage ), 'warning' );
      }
      self::$_callCount[ $scope ]++;
    }
    else
    {
      $this->_main->ReadLessText( $article, self::$_callCount[ $scope ], 'plg_content_readlesstextagain', '#__readlesstextagain' );
    }
  }

  private static function _CorrectParams( $params )
  {
    /* Fetch the readlesstext params. */
    $corePlugin = JPluginHelper::getPlugin( 'content', 'readlesstext' );
    if ( $corePlugin )
    {
      $mainParams = new JRegistry( $corePlugin->params );

      /* Format the own params */
      if ( !/*NOT*/$params instanceof JRegistry )
      {
        $p = new JRegistry;
        $p->loadString( $params );
        $params = $p;
      }

      /* Fetch the overridden params */
      $nonDefaultParams = array();
      foreach ( $params->toArray() as $key => $value )
      {
        if ( ( $value !== self::_defaultString ) and ( $value !== self::_defaultInt ) )
        {
          $nonDefaultParams[ $key ] = $value;
        }
      }
      $overridenParams = new JRegistry();
      $overridenParams->loadArray( $nonDefaultParams );

      /* Overwrite the core params with the overridden params */
      $params = $mainParams;
      $params->merge( $overridenParams );
    }

    return $params;
  }

  private $_main = null;
  private static $_callCount = array();
  const _defaultInt = -1;
  const _defaultString = 'Default';
}

?>
