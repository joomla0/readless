<?php
/**
 * @package readlesstext
 * @copyright __YEAR__ Parvus
 * @license http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://joomlacode.org/gf/project/cutoff/
 * @author Parvus
 *
 * readless is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * readless is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with readless. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @version $Id$
 */

defined( '_JEXEC' ) or die;

class plgContentReadlesstextagainInstallerScript
{
  /**
   * Constructor
   * @param JAdapterInstance $adapter The object responsible for running this script
   */
  public function __constructor(JAdapterInstance $adapter)
  {
    //void
  }

  /**
   * Called before any type of action
   * @param string $route Which action is happening (install|uninstall|discover_install)
   * @param JAdapterInstance $adapter The object responsible for running this script
   * @return boolean True on success
   */
  public function preflight($route, JAdapterInstance $adapter)
  {
    return true;
  }

  /**
   * Called after any type of action
   * @param string $route Which action is happening (install|uninstall|discover_install)
   * @param JAdapterInstance $adapter The object responsible for running this script
   * @return boolean True on success
   */
  public function postflight($route, JAdapterInstance $adapter)
  {
    $this->_CheckDependency();
    return true;
  }

  /**
   * Called on installation
   * @param JAdapterInstance $adapter The object responsible for running this script
   * @return boolean True on success
   */
  public function install(JAdapterInstance $adapter)
  {
    $this->_CreateTable();
    return true;
  }

  /**
   * Called on update
   * @param JAdapterInstance $adapter The object responsible for running this script
   * @return boolean True on success
   */
  public function update(JAdapterInstance $adapter)
  {
    $this->_CreateTable();
    return true;
  }

  /**
   * Called on uninstallation
   * @param JAdapterInstance $adapter The object responsible for running this script
   */
  public function uninstall(JAdapterInstance $adapter)
  {
    $this->_DestroyTable();
    return true;
  }

  private function _CheckDependency()
  {
    JPlugin::loadLanguage( 'plg_content_readlesstextagain' );
    $app = JFactory::getApplication();

    $db = JFactory::getDBO();
    $db->setQuery( self::_sql );
    $params = $db->loadResult();
    if ( $params )
    {
      $p = new JRegistry;
      $p->loadString( $params );
      $params = $p;

      $version = $params->get( 'version' );
      if ( $version )
      {
        $msg = JText::sprintf( 'PLG_CONTENT_READLESSTEXTAGAIN_DEPENDENCY_FOUND', $version );
        $app->enqueueMessage( $msg, 'info' );
        $revision = 0;

        /* The last part of the version string is the revision number.
         * First find all parts that are not forming a number.
         * Replace them all with the same character, e.g. a dot
         * Then find all parts that are not that character
         * In the array of numbers found this way, the last number will
         * be the revision number.
         */
        $parts = array();
        $part = strtok( $version, '0123456789' );
        while ( $part !== false )
        {
          $parts[] = $part;
          $part = strtok( '0123456789' );
        }
        $s = str_replace( $parts, '.', $version );
        $part = strtok( $s, '.' );
        while ( $part !== false )
        {
          $revision = $part;
          $part = strtok( '.' );
        }
        if ( $revision < self::_minimumRequiredReadlesstextRevision )
        {
          $msg = JText::sprintf( 'PLG_CONTENT_READLESSTEXTAGAIN_DEPENDENCY_FAILED', $version );
          $app->enqueueMessage( $msg, 'warning' );
        }
//        else if ( $majorVersion > self::_minimumRequiredReadlesstextRevision )
//        {
//          $msg = JText::_( 'PLG_CONTENT_READLESSTEXTAGAIN_DEPENDENCY_UNSURE' );
//          $app->enqueueMessage( $msg, 'warning' );
//        }
        else
        {
          /* Ok. */
        }
      }
      else
      {
        $msg = JText::sprintf( 'PLG_CONTENT_READLESSTEXTAGAIN_DEPENDENCY_UNSURE', '0' );
        $app->enqueueMessage( $msg, 'warning' );
      }
    }
    else
    {
      $msg = JText::_( 'PLG_CONTENT_READLESSTEXTAGAIN_DEPENDENCY_MISSING' );
      $app->enqueueMessage( $msg, 'warning' );
    }
  }

  private function _CreateTable()
  {
    $db = JFactory::getDBO();
    $db->setQuery( self::_createTableSql );
    $db->query();
  }

  private function _DestroyTable()
  {
    $db = JFactory::getDBO();
    $db->setQuery( self::_destroyTableSql );
    $db->query();
  }

  const _sql = "SELECT manifest_cache FROM `#__extensions` WHERE element='readlesstext'";
  const _version = '__VERSION__';
  const _minimumRequiredReadlesstextRevision = 243;

  const _createTableSql = "CREATE TABLE IF NOT EXISTS `#__readlesstextagain` (
    `id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `rtable` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'The component name where the item resides in. Also called option',
    `rid` INT(10) NOT NULL COMMENT 'The unique id of the item in that component. e.g. the article id for com_content',
    `hash` VARCHAR(255) DEFAULT '' COMMENT 'The fingerprint of the full item text.',
    `char` INTEGER UNSIGNED DEFAULT 0 COMMENT 'Count in the full item text.',
    `word` INTEGER UNSIGNED DEFAULT 0 COMMENT 'Count in the full item text.',
    `sentence` INTEGER UNSIGNED DEFAULT 0 COMMENT 'Count in the full item text.',
    `paragraph` INTEGER UNSIGNED DEFAULT 0 COMMENT 'Count in the full item text.',
    `image_tag_start_pos` INTEGER UNSIGNED DEFAULT 0 COMMENT 'Start position of the image tag where the thumbnail was created from.',
    `image_tag_length` INTEGER UNSIGNED DEFAULT 0 COMMENT 'Length of the image tag in nr of UTF8 chars.',
    `image_url` VARCHAR(1023) NOT NULL DEFAULT '' COMMENT 'Url to the image where the thumbnail was created from.',
    `thumbnail_url` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Url to the thumbnail.',
    `last_update` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Debug information.',
    UNIQUE (`rtable`, `rid`) ) DEFAULT CHARSET=utf8;";

  const _destroyTableSql = "DROP TABLE IF EXISTS `#__readlesstextagain`";
}

?>

