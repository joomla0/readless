#!/usr/bin/python3

# @copyright 2013 Dries
# @license http://www.gnu.org/licenses/gpl-3.0.html
# @author Dries
#
# This file is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this file. If not, see <http://www.gnu.org/licenses/>.

import sys
import csv

''' iniSort will sort an ini language file, maintaining the sections that are created by means of blank lines.
'''

def About():
    print('''{0}

Usage:
  {1} filename.ini

- filename.ini[in, out]: the name of the file that contains the ini language strings, that need to get sorted.
''').format('this module info', sys.argv[0])

class IniGroup:
  def __init__(self):
    self.comments = []
    self.ini = {}
  
  def Add(self, line):
    line = line.strip()
    kv = line.split('=', 1)
    if len(line) == 0:
      if len(self.ini) == 0:
        if len(self.comments) > 0 and len(self.comments[-1]) == 0:
          self.comments.append(line)
        else:
          pass # remove multiple contiguous empty lines right after a comment
      else:
        pass # remove empty lines between two ini strings
    else:
      if line[0] == ';':
        if len(self.ini) == 0:
          self.comments.append(line)
        else:
          raise Exception('is a comment and belongs to a new group')
      elif len(kv) == 2:
        self.ini[kv[0]] = kv[1]
      else:
        raise Exception('is not a comment and not a valid translation line')

  def __str__(self):
    return '\n'.join(self.comments
                     + [k + '="' + self.ini[k].strip('"') + '"' for k in sorted(self.ini.keys())]
                     ) + "\n" 

def IniSort(filename):
  iniGroups = []
  iniGroups.append(IniGroup())
  with open(filename, 'r') as f:
    for line in f.readlines():
      try:
        iniGroups[-1].Add(line)
      except:
        iniGroups.append(IniGroup())
        iniGroups[-1].Add(line)
  with open(filename, 'w') as f:
    f.write('\n'.join([str(x) for x in iniGroups]) + '\n')

if __name__ == '__main__':
  if (sys.version_info[0] <= 2):
    sys.exit('Run this script using python3 or higher.')
  if len(sys.argv) == 1:
    About()
    sys.exit(1)
  elif len(sys.argv) == 2:
    IniSort(sys.argv[1])
  else:
    sys.exit('Incorrect number of arguments. Run {0} without any arguments for help'.format(sys.argv[0]))
