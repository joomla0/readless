#!/bin/bash

# sanity checks
declare -A checks
checks[__DATE_]=1
checks[__VERSION__]=4
checks[__YEAR__]=8
checks[echo]=1
for key in "${!checks[@]}"
do
  export __COUNT__=`cat *.php *.xml | grep -c -E "$key"`
  if test "${__COUNT__}" -ne "${checks[$key]}"; then
    echo "WARNING. Found $key ${__COUNT__} times; expected ${checks[$key]} times."
  fi;
done;

__NAME__=readlesstext

# ........... in the installer files
#             will be replaced with ...
# __DATE__                          today's date in the format YYYY-MM-DD
# __YEAR__                          today's year in the format YYYY
# __VERSION__                       a string with the version number and the revision number,
#                                   in that order. The last number must be the revision number -
#                                   the installer script of read less text again depends on this
__YEAR__=`date +"%Y"`
__DATE__=`date --utc +"%B %Y"`
__REVISION__=`svnversion --no-newline`
__VERSION__=`head -1 version | tr -d "\r\n"`

rm -R assembled 2>/dev/null
mkdir --parents assembled/language/en-GB 2>/dev/null
mkdir --parents assembled/language/nl-NL 2>/dev/null
cp ${__NAME__}*.php assembled/
cp script.php assembled/
cp language/en-GB/*.ini assembled/language/en-GB/
cp language/nl-NL/*.ini assembled/language/nl-NL/
cp ../license assembled/
cp ../gpl.txt assembled/
cp ../index.html assembled/
cp ${__NAME__}.xml assembled/

cd assembled
cloc --quiet .

find . -type f -exec sed -i "s/__YEAR__/${__YEAR__}/g" {} \;
find . -type f -exec sed -i "s/__DATE__/${__DATE__}/g" {} \;
find . -type f -exec sed -i "s/__REVISION__/${__REVISION__}/g" {} \;
find . -type f -exec sed -i "s/__VERSION__/${__VERSION__} (r${__REVISION__})/g" {} \;

rm ../${__NAME__}_${__VERSION__}*.zip 2>/dev/null
zip --quiet --archive-comment --latest-time --recurse-paths -9 "../../${__NAME__}_${__VERSION__}_r${__REVISION__}.zip" * < license
cd ..

rm -R assembled 2>/dev/null
zip --test "../${__NAME__}_${__VERSION__}_r${__REVISION__}.zip"
